package com.example.salary_info2.database;

import java.util.List;
import java.sql.Timestamp;
import java.util.Date;

public class DayService {
	
	private static DayDao dayDao;
	
	public DayService() {
		if (dayDao == null) dayDao = new DayDao();
	}
	
	public void persist(Day entity) {
		dayDao.openCurrentSessionwithTransaction();
		dayDao.persist(entity);
		dayDao.closeCurrentSessionwithTransaction();
	}
	
	public void update(Day entity) {
		dayDao.openCurrentSessionwithTransaction();
		dayDao.update(entity);
		dayDao.closeCurrentSessionwithTransaction();
	}
	
	public void saveOrUpdate(Day entity) {
		dayDao.openCurrentSessionwithTransaction();
		dayDao.saveOrUpdate(entity);
		dayDao.closeCurrentSessionwithTransaction();
	}
	
	public Day findById(int id) {
		dayDao.openCurrentSession();
		Day day = dayDao.findById(id);
		dayDao.closeCurrentSession();
		return day;
	}
	
	public void delete(int id) {
		dayDao.openCurrentSessionwithTransaction();
		Day day = dayDao.findById(id);
		dayDao.delete(day);
		dayDao.closeCurrentSessionwithTransaction();
	}
	
	public List<Day> findAll() {
		dayDao.openCurrentSession();
		List<Day> days = dayDao.findAll();
		dayDao.closeCurrentSession();
		return days;
	}
	
	public void deleteDay(Date date) {
		dayDao.openCurrentSessionwithTransaction();
		dayDao.deleteDay(date);
		dayDao.closeCurrentSessionwithTransaction();
	}
	
	public void deleteAll() {
		dayDao.openCurrentSessionwithTransaction();
		dayDao.deleteAll();
		dayDao.closeCurrentSessionwithTransaction();
	}
	
	public DayDao dayDao() {
		return dayDao;
	}
}