package com.example.salary_info2.database;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "day")
public class Day {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(unique = true, name = "date", columnDefinition="DATE")
	private Date date;
	@Column(name = "rate")
	private float rate;
	@Column(name = "hours")
	private float hours;
	@Column(name = "bonus")
	private float bonus;
	
	public Day() {}

	public Day(Date date, float rate, float hours, float bonus) {
		super();
		this.date = date;
		this.rate = rate;
		this.hours = hours;
		this.bonus = bonus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		if (this.id != id)
			this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		if (!this.date.equals(date))
			this.date = date;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		if (this.rate != rate)
			this.rate = rate;
	}

	public float getHours() {
		return hours;
	}

	public void setHours(float hours) {
		if (this.hours != hours)
			this.hours = hours;
	}

	public float getBonus() {
		return bonus;
	}

	public void setBonus(float bonus) {
		if (this.bonus != bonus)
			this.bonus = bonus;
	}
	
	public float getSalary() {
		return (rate * hours) + bonus;
	}
}