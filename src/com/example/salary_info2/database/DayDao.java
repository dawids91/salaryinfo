package com.example.salary_info2.database;

import java.util.List;
import java.sql.Timestamp;
import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.transform.Transformers;

public class DayDao {
	
	private Session currentSession;
	private Transaction currentTransaction;
	
	public DayDao() {}
	
	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}
	
	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}
	
	public void closeCurrentSession() {
		currentSession.close();
	}
	
	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}
	
	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure().addAnnotatedClass(Day.class);;
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}
	
	public Session getCurrentSession() {
		return currentSession;
	}
	
	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}
	
	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}
	
	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}
	
	public void persist(Day entity) {
		getCurrentSession().save(entity);
	}
	
	public void update(Day entity) {
		getCurrentSession().update(entity);
	}
	
	public void saveOrUpdate(Day entity) {
		getCurrentSession().saveOrUpdate(entity);
	}
	
	public Day findById(int id) {
		Day day = (Day) getCurrentSession().get(Day.class, id);
		return day;
	}
	
	public void delete(Day entity) {
		getCurrentSession().delete(entity);
	}
	
	public void deleteDay(Date date) {
		Timestamp dateSQL = new Timestamp(date.getTime());
		Day entity = (Day) getCurrentSession().createQuery("FROM Day WHERE date = '" + dateSQL + "'").uniqueResult();
		if (entity != null)
			getCurrentSession().delete(entity);
	}
	
	@SuppressWarnings("unchecked")
	public List<Day> findAll() {
		List<Day> days = (List<Day>) getCurrentSession().createQuery("FROM Day").list();
		return days;
	}
	
	public void deleteAll() {
		List<Day> entityList = findAll();
		for(Day entity : entityList) {
			delete(entity);
		}
	}
}