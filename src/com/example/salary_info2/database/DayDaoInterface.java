package com.example.salary_info2.database;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public interface DayDaoInterface<T, Id extends Serializable> {
	public void persist(T entity);
	public void saveOrUpdate(T entity);
	public void update(T entity);
	public T findById(Id id);
	public void delete(T entity);
	public List<T> findAll();
	public void deleteDay(Date date);
	public void deleteAll();
}