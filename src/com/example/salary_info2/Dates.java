package com.example.salary_info2;

import java.util.Calendar;
import java.util.Date;

public class Dates {
	
	private static Date newDate;
	
	public static Date getNewDate() {
		return newDate = getDate();
	}
	
	private static Date getDate() {
		if (newDate == null) {
			return getTodayDate();
		}
		return getFollowingDate();
	}
	
	public static Date getTodayDate() {
		return Calendar.getInstance().getTime();
	}
	
	public static Date getTommorowDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		return calendar.getTime();
	}
	
	public static Date getFollowingDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		calendar.add(Calendar.DATE, 1);
		return calendar.getTime();
	}
}
