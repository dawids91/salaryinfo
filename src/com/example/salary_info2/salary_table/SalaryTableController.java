package com.example.salary_info2.salary_table;

import com.example.salary_info2.SalaryInfoUI;

public class SalaryTableController {

	private SalaryTableLayout layout;
	private SalaryTable salaryTable;
	private SalaryInfoUI view;
	
	public SalaryTableController(SalaryInfoUI view) {
		this.view = view;
		salaryTable = new SalaryTable();
		init();
	}
	
	public SalaryTableLayout getLayout() {
		return layout;
	}
	
	public SalaryTable getTable() {
		return salaryTable;
	}
	
	private void init() {
		layout = new SalaryTableLayout();
		layout.addComponent(salaryTable);
		SalaryTableCheckbox salaryTableCheckbox = new SalaryTableCheckbox(salaryTable);
		layout.addComponent(salaryTableCheckbox);
		layout.addComponent(new CreateDayButton(salaryTable, salaryTableCheckbox));
		layout.addComponent(new CreateDaysButton(view));
		layout.addComponent(new DeleteDaysButton(view));
		layout.addComponent(new PersistDaysButton(salaryTable.getSaveOrUpdateButtons()));
	}
}