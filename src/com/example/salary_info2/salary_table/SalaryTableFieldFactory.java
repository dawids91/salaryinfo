package com.example.salary_info2.salary_table;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import com.example.salary_info2.Calculations;
import com.vaadin.data.Container;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Field;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TableFieldFactory;
import com.vaadin.ui.TextField;

public class SalaryTableFieldFactory implements TableFieldFactory {
	
	private String[] COLUMNS;
	private SalaryTable salaryTable;

	public SalaryTableFieldFactory(String[] COLUMNS, SalaryTable salaryTable) {
		this.COLUMNS = COLUMNS;
		this.salaryTable = salaryTable;
	}
	
	@Override
	public Field<?> createField(Container container, Object itemId, Object propertyId, Component uiContext) {
		if (!propertyId.equals(COLUMNS[0])) {
			TextField field = new TextField();
			field.addTextChangeListener(event -> {
				updateContainerDataSource(container, itemId, propertyId, event.getText());
				updateSalary(container, itemId);
				updateDatesFooter(propertyId);
				updateRateFooter(propertyId);
				updateHoursFooter(propertyId);
				updateBonusFooter(propertyId);
				updateSalaryFooter();
			});
			return field;
		} else {
			DateField field = new DateField();
			field.addValueChangeListener(event -> {
				updateDatesFooter(propertyId);
			});
			return field;
		}
	}
	
	private void updateContainerDataSource(Container container, Object itemId, Object propertyId, String newValue) {
		if (newValue.isEmpty())
			container.getContainerProperty(itemId, propertyId).setValue(0f);
		else {
			container.getContainerProperty(itemId, propertyId).setValue(Calculations.convertTableStringToFloat(newValue));
		}
	}
	
	private void updateSalary(Container container, Object itemId) {
		float newSalary = getUpdatedSalary(container, itemId);
		container.getContainerProperty(itemId, COLUMNS[4]).setValue(newSalary);
	}

	private float getUpdatedSalary(Container container, Object itemId) {
		return salaryTable.getRate(itemId) * salaryTable.getHours(itemId) + salaryTable.getBonus(itemId);
	}
	
	private void updateDatesFooter(Object propertyId) {
		if (propertyId.equals(COLUMNS[0])) {
			salaryTable.refreshDatesFooter();
		}
	}
	
	private void updateRateFooter(Object propertyId) {
		if (propertyId.equals(COLUMNS[1])) {
			salaryTable.refreshRateFooter();
		}
	}
	
	private void updateHoursFooter(Object propertyId) {
		if (propertyId.equals(COLUMNS[2])) {
			salaryTable.refreshHoursFooter();
		}
	}
	
	private void updateBonusFooter(Object propertyId) {
		if (propertyId.equals(COLUMNS[3])) {
			salaryTable.refreshBonusFooter();
		}
	}
	
	private void updateSalaryFooter() {
		salaryTable.refreshSalaryFooter();
	}
}
