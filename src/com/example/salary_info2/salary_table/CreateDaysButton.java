package com.example.salary_info2.salary_table;

import com.example.salary_info2.SalaryInfoUI;
import com.example.salary_info2.ViewState;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class CreateDaysButton extends Button {
	
	public CreateDaysButton(SalaryInfoUI view) {
		super("Generowanie dni pracy");
		addClickListener(event -> {
			view.CURRENT = ViewState.ADD;
			view.refresh(null);
			Notification.show("Wybierz kluczowe daty, wype�nij potrzebne dane. Nast�pnie kliknij przycisk 'OK'.");
		});
	}
}
