package com.example.salary_info2.salary_table;

import com.vaadin.ui.CheckBox;

public class SalaryTableCheckbox extends CheckBox {
	
	public SalaryTableCheckbox(SalaryTable salaryTable) {
		super("Edycja tabeli", false);
		addValueChangeListener(event -> {
			salaryTable.setEditable(SalaryTableCheckbox.this.getValue());
		});
	}
}
