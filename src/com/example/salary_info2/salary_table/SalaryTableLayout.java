package com.example.salary_info2.salary_table;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.VerticalLayout;

public class SalaryTableLayout extends VerticalLayout {

	public SalaryTableLayout() {
		super();
		setProperties();
	}

	private void setProperties() {
		setMargin(true);
		setDefaultComponentAlignment(Alignment.TOP_CENTER);
	}
}
