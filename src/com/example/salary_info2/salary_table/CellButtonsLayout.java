package com.example.salary_info2.salary_table;

import com.example.salary_info2.database.Day;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;

public class CellButtonsLayout extends HorizontalLayout {
	
	private UpdateDayButton updateDayButton;
	
	public CellButtonsLayout(SalaryTable salaryTable, Day day, int ID) {
		setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		updateDayButton = new UpdateDayButton(salaryTable, day, ID);
		addComponent(updateDayButton);
		addComponent(new DeleteDayButton(salaryTable, day, ID));
	}
	
	public Button getUpdateButton() {
		return updateDayButton;
	}
}
