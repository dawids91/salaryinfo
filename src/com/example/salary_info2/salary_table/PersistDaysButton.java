package com.example.salary_info2.salary_table;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class PersistDaysButton extends Button {
	
	public PersistDaysButton(List<Button> saveOrUpdateButtons) {
		super("Zapisz ca�� tabel�");
		addClickListener(event -> {
			for (Button updateButton : saveOrUpdateButtons) {
				updateButton.click();
				Notification.show("Zaktualizowano tabel�");
			}
		});
	}
}
