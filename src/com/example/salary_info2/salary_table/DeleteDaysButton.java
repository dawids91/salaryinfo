package com.example.salary_info2.salary_table;

import com.example.salary_info2.SalaryInfoUI;
import com.example.salary_info2.ViewState;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class DeleteDaysButton extends Button {
	
	public DeleteDaysButton(SalaryInfoUI view) {
		super("Usuwanie dni w przedziale dat");
		addClickListener(event -> {
			view.CURRENT = ViewState.DELETE;
			view.refresh(null);
			Notification.show("Wybierz przedzia� dat, w kt�rym dni zostan� usuni�te. Nast�pnie kliknij przycisk 'OK'.");
		});
	}
}
