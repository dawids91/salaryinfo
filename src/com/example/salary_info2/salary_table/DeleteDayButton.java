package com.example.salary_info2.salary_table;

import com.example.salary_info2.database.Day;
import com.example.salary_info2.database.DayService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class DeleteDayButton extends Button {
	
	public DeleteDayButton(SalaryTable salaryTable, Day day, int ID) {
		super("Usu�");
		DayService dayService = new DayService();
		addClickListener(event -> {
			salaryTable.removeItem(ID);
			salaryTable.refreshFooter();
			salaryTable.refreshTableLength();
			dayService.delete(day.getId());
			Notification.show("Dzie� usuni�ty.");
		});
	}
}
