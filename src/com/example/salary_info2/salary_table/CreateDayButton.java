package com.example.salary_info2.salary_table;

import java.util.Date;

import com.example.salary_info2.Dates;
import com.example.salary_info2.database.Day;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class CreateDayButton extends Button {
	
	public CreateDayButton(SalaryTable salaryTable, SalaryTableCheckbox salaryTableCheckbox) {
		super("Dodaj nowy dzie�");
		addClickListener(event -> {
			Date date = Dates.getNewDate();
			Day newDay = new Day(date, 0, 0, 0);
			salaryTable.addRow(newDay);
			salaryTable.refreshFooter();
			salaryTable.refreshTableLength();
			salaryTableCheckbox.setValue(true);
			Notification.show("Dodaj kolejne dni lub wype�nij dodany dzie�. Nast�pnie kliknij przycisk 'Zapisz dzie�' lub 'Zapisz wszystkie dni'.");
		});
	}
}
