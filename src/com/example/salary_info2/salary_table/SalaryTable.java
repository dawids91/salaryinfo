package com.example.salary_info2.salary_table;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.example.salary_info2.Calculations;
import com.example.salary_info2.HTML;
import com.example.salary_info2.VaadinTable;
import com.example.salary_info2.database.Day;
import com.example.salary_info2.database.DayService;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.converter.StringToDateConverter;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TableFieldFactory;
import com.vaadin.ui.TextField;

public class SalaryTable extends VaadinTable {

	public String START_DATE;
	public String END_DATE;
	public String AVERAGE_RATE;
	public String TOTAL_HOURS;
	public String AVERAGE_HOURS;
	public String TOTAL_BONUS;
	public String AVERAGE_BONUS;
	public String TOTAL_SALARY;
	public String AVERAGE_SALARY;
	private List<Button> saveOrUpdateButtons;
	
	public SalaryTable() {
		super("Zarobki");
		saveOrUpdateButtons = new ArrayList<Button>();
		defineColumns();
		createColumns();
		convertDates();
		fillTable();
		setProperties();
		setColumnAlignments(ALIGNMENT.CENTER);
		createFooter();
	}
	
	public float getRate(Object itemId) {
		return (float) getContainerProperty(itemId, COLUMNS[1]).getValue();
	}
	
	public float getHours(Object itemId) {
		return (float) getContainerProperty(itemId, COLUMNS[2]).getValue();
	}
	
	public float getBonus(Object itemId) {
		return (float) getContainerProperty(itemId, COLUMNS[3]).getValue();
	}
	
	public float getSumFromColumn(String column) {
		Collection<?> IDs = getItemIds();
		float sum = 0f;
		for (Object ID : IDs) {
			String value = getContainerProperty(ID, column).getValue().toString();
			sum += Calculations.convertTableStringToFloat(value);
		}
		return sum;
	}
	
	public void refreshFooter() {
		refreshDatesFooter();
		refreshRateFooter();
		refreshHoursFooter();
		refreshBonusFooter();
		refreshSalaryFooter();
	}
	
	public void refreshDatesFooter() {
		START_DATE = datesRange().get("FIRST DATE");
		END_DATE = datesRange().get("LAST DATE");
		setColumnFooter(COLUMNS[0], HTML.datesRange(START_DATE, END_DATE));
	}
	
	public void refreshRateFooter() {
		AVERAGE_RATE = String.valueOf(getSumFromColumn(COLUMNS[1]) / items.size());
		setColumnFooter(COLUMNS[1], HTML.averageRate(AVERAGE_RATE));
	}
	
	public void refreshHoursFooter() {
		TOTAL_HOURS = String.valueOf(getSumFromColumn(COLUMNS[2]));
		AVERAGE_HOURS = String.valueOf(getSumFromColumn(COLUMNS[2]) / items.size());
		setColumnFooter(COLUMNS[2], HTML.totalHours(TOTAL_HOURS) + HTML.lineBreak() + HTML.averageHours(AVERAGE_HOURS));
	}
	
	public void refreshBonusFooter() {
		TOTAL_BONUS = String.valueOf(getSumFromColumn(COLUMNS[3]));
		AVERAGE_BONUS = String.valueOf(getSumFromColumn(COLUMNS[3]) / items.size());
		setColumnFooter(COLUMNS[3], HTML.totalBonus(TOTAL_BONUS) + HTML.lineBreak() + HTML.averageBonus(AVERAGE_BONUS));
	}
	
	public void refreshSalaryFooter() {
		TOTAL_SALARY = String.valueOf(getSumFromColumn(COLUMNS[4]));
		AVERAGE_SALARY = String.valueOf(getSumFromColumn(COLUMNS[4]) / items.size());
		setColumnFooter(COLUMNS[4], HTML.totalSalary(TOTAL_SALARY) + HTML.lineBreak() + HTML.averageSalary(AVERAGE_SALARY));
	}
	
	public List<Button> getSaveOrUpdateButtons() {
		return saveOrUpdateButtons;
	}
	
	@Override
	public <T> boolean addRow(T model) {
		try {
			Day day = (Day) model;
			int ID = (int) this.addItem();
			Item row = getItem(ID);
			row.getItemProperty(COLUMNS[0]).setValue(day.getDate());
			row.getItemProperty(COLUMNS[1]).setValue(day.getRate());
			row.getItemProperty(COLUMNS[2]).setValue(day.getHours());
			row.getItemProperty(COLUMNS[3]).setValue(day.getBonus());
			row.getItemProperty(COLUMNS[4]).setValue(day.getSalary());
			CellButtonsLayout buttons = new CellButtonsLayout(this, day, ID);
			saveOrUpdateButtons.add(buttons.getUpdateButton());
			row.getItemProperty(COLUMNS[5]).setValue(buttons);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			Notification.show("B��d dodawania wiersza tabeli Vaadin: " + ex);
			return false;
		}
	}
	
	public boolean addRow(Day day, int id) {
		try {
			CellButtonsLayout buttons = new CellButtonsLayout(this, day, id);
			saveOrUpdateButtons.add(buttons.getUpdateButton());
			addItem(new Object[] {
					day.getDate(), day.getRate(), day.getHours(), day.getBonus(), day.getSalary(), buttons
					}, day.getId());
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			Notification.show("B��d dodawania wiersza tabeli Vaadin: " + ex);
			return false;
		}
	}
	
	public boolean removeRow(Date lookupDate) {
		List<Integer> IDs = (List<Integer>) this.getItemIds();
		for (int ID : IDs) {
			Date currentDate = (Date) this.getContainerProperty(ID, COLUMNS[0]).getValue();
			if (currentDate.compareTo(lookupDate) == 0) {
				this.removeItem(ID);
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected void defineColumns() {
		COLUMNS = new String[] {
				"Data",
				"Stawka godzinowa [z�]",
				"Liczba godzin [h]",
				"Premia [z�]",
				"Wynagrodzenie [z�]",
				"Operacje"};
	}
	
	@Override
	protected void createColumns() {
		addContainerProperty(COLUMNS[0], Date.class, null);
		setColumnWidth(COLUMNS[0], 250);
		addContainerProperty(COLUMNS[1], Float.class, null);
		setColumnWidth(COLUMNS[1], 200);
		addContainerProperty(COLUMNS[2], Float.class, null);
		setColumnWidth(COLUMNS[2], 200);
		addContainerProperty(COLUMNS[3], Float.class, 0);
		setColumnWidth(COLUMNS[3], 200);
		addContainerProperty(COLUMNS[4], Float.class, null);
		setColumnWidth(COLUMNS[4], 200);
		addContainerProperty(COLUMNS[5], HorizontalLayout.class, null);
		setColumnWidth(COLUMNS[5], 300);
		
		setTableFieldFactory(new SalaryTableFieldFactory(COLUMNS, this));
	}

	private void fillTable() {
		DayService dayService = new DayService();
		List<Day> days = dayService.findAll();
		
		for (Day day : days) {
			addRow(day, day.getId());
		}
		
		AVERAGE_RATE = Calculations.getRateAverage(days);
		TOTAL_HOURS = Calculations.getHoursSum(days);
		AVERAGE_HOURS = Calculations.getHoursAverage(days);
		TOTAL_BONUS = Calculations.getBonusSum(days);
		AVERAGE_BONUS = Calculations.getBonusAverage(days);
		TOTAL_SALARY = Calculations.getSalarySum(days);
		AVERAGE_SALARY = Calculations.getSalaryAverage(days);
	}
	
	@Override
	protected void setProperties() {
		setPageLength(this.size());
		setSortContainerPropertyId("Data");
		setSortAscending(true);
		setSelectable(true);
		setEditable(false);
	}
	
	private void convertDates() {
		setConverter(COLUMNS[0], new StringToDateConverter() {
			@Override
			public DateFormat getFormat(Locale locale) {
				Locale.setDefault(locale);
				return new SimpleDateFormat("dd MMMM yyyy");
			}
		});
	}
	
	private void createFooter() {
		START_DATE = datesRange().get("FIRST DATE");
		END_DATE = datesRange().get("LAST DATE");
		
		setFooterVisible(true);
		setColumnFooter(COLUMNS[0], HTML.datesRange(START_DATE, END_DATE));
		setColumnFooter(COLUMNS[1], HTML.averageRate(AVERAGE_RATE));
		setColumnFooter(COLUMNS[2], HTML.totalHours(TOTAL_HOURS) + HTML.lineBreak() + HTML.averageHours(AVERAGE_HOURS));
		setColumnFooter(COLUMNS[3], HTML.totalBonus(TOTAL_BONUS) + HTML.lineBreak() + HTML.averageBonus(AVERAGE_BONUS));
		setColumnFooter(COLUMNS[4], HTML.totalSalary(TOTAL_SALARY) + HTML.lineBreak() + HTML.averageSalary(AVERAGE_SALARY));
	}
	
	private HashMap<String,String> datesRange() {
		Collection<?> IDs = getItemIds();
		List<Date> dates = new ArrayList<Date>();
		for (Object ID : IDs) {
			dates.add((Date) getContainerProperty(ID, COLUMNS[0]).getValue());
		}
		Collections.sort(dates);
		
		String firstDate = Calculations.convertDateToString(dates.get(0));
		String lastDate = Calculations.convertDateToString(dates.get(dates.size()-1));
		
		HashMap<String,String> output = new HashMap<String,String>();
		output.put("FIRST DATE", firstDate);
		output.put("LAST DATE", lastDate);
		
		return output;
	}
}
