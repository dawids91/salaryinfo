package com.example.salary_info2.salary_table;

import java.util.Date;

import com.example.salary_info2.database.Day;
import com.example.salary_info2.database.DayService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class UpdateDayButton extends Button {

	public UpdateDayButton(SalaryTable salaryTable, Day day, int ID) {
		super("Zapisz");
		DayService dayService = new DayService();
		addClickListener(event -> {
			day.setDate((Date) salaryTable.getContainerProperty(ID, "Data").getValue());
			day.setRate((float) salaryTable.getContainerProperty(ID, "Stawka godzinowa [z�]").getValue());
			day.setHours((float) salaryTable.getContainerProperty(ID, "Liczba godzin [h]").getValue());
			day.setBonus((float) salaryTable.getContainerProperty(ID, "Premia [z�]").getValue());
			try {
				dayService.saveOrUpdate(day);
				Notification.show("Dzie� zapisany.");
			} catch (Exception ex) {
				ex.printStackTrace();
				Notification.show("Daty w tabeli musz� by� unikalne");
			}
		});
	}
}
