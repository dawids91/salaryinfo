package com.example.salary_info2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.example.salary_info2.database.Day;
import com.vaadin.ui.Notification;

import java.text.DateFormat;

public class Calculations {

	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
	private static final String RegexAllWhitespacesUTF8 = "[ \\t\\xA0\\u1680\\u180e\\u2000-\\u200a\\u202f\\u205f\\u3000]";
	
	public static String getRateAverage(List<Day> days) {
		float sum = 0f;
		for(Day day : days) {
			sum += day.getRate();
		}
		return String.valueOf(sum / days.size());
	}
	
	public static String getHoursSum(List<Day> days) {
		float sum = 0;
		for(Day day : days) {
			sum += day.getHours();
		}
		return String.valueOf(sum);
	}
	
	public static String getHoursAverage(List<Day> days) {
		float sum = 0f;
		for(Day day : days) {
			sum += day.getHours();
		}
		return String.valueOf(sum / days.size());
	}
	
	public static String getBonusSum(List<Day> days) {
		float sum = 0;
		for(Day day : days) {
			sum += day.getBonus();
		}
		return String.valueOf(sum);
	}
	
	public static String getBonusAverage(List<Day> days) {
		float sum = 0f;
		for(Day day : days) {
			sum += day.getBonus();
		}
		return String.valueOf(sum / days.size());
	}
	
	public static String getSalarySum(List<Day> days) {
		float sum = 0;
		for (Day day : days) {
			sum += day.getSalary();
		}
		return String.valueOf(sum);
	}
	
	public static String getSalaryAverage(List<Day> days) {
		float salarySum = Float.valueOf(getSalarySum(days));
		return String.valueOf(salarySum / days.size());
	}
	
	public static Date dateFormatSQL(Date date) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		return dateFormat.parse(dateFormat.format(calendar.getTime()));
	}
	
	public static String convertDateToString(Date date) {
		if (date != null)
			return dateFormatter.format(date);
		else
			return "";
	}
	
	public static Date convertStringToDate(String date) {
		try {
			return dateFormatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date();
		}
	}
	
	public static float convertTableStringToFloat(String input) {
		try {
			input = input.replaceAll(RegexAllWhitespacesUTF8,"");
			return Float.valueOf(input);
		} catch (Exception ex) {
			ex.printStackTrace();
			Notification.show("B��d konwersji liczby w kom�rce: " + ex);
			return 0f;
		}
	}
	
	public static int daysBetween(Date date1, Date date2) {
        return (int) ((date2.getTime() - date1.getTime()) / (1000 * 60 * 60 * 24));
	}
	
	public static Date getFirstDayOfMonth() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
	
	public static Date getLastDayOfMonth() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
}