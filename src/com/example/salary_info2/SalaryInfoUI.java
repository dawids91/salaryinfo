package com.example.salary_info2;

import javax.servlet.annotation.WebServlet;

import com.example.salary_info2.creation_table.CreateDaysController;
import com.example.salary_info2.deletion_table.DeleteDaysController;
import com.example.salary_info2.salary_table.SalaryTableController;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("salary_info2")
public class SalaryInfoUI extends UI {

	public ViewState CURRENT;
	private SalaryTableController salaryTable;
	private DeleteDaysController deleteDays;
	private CreateDaysController createDays;
	
	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = SalaryInfoUI.class, widgetset = "com.example.salary_info2.widgetset.Salary_info2Widgetset")
	public static class Servlet extends VaadinServlet {}
	
	@Override
	protected void init(VaadinRequest request) {
		CURRENT = ViewState.MAIN;
		salaryTable = new SalaryTableController(this);
		setContent(salaryTable.getLayout());
	}

	@Override
	public void refresh(VaadinRequest request) {
		super.refresh(request);
		if (CURRENT == ViewState.MAIN) {
			setContent(salaryTable.getLayout());
		} else if (CURRENT == ViewState.ADD) {
			if (createDays == null) {
				createDays = new CreateDaysController(this, salaryTable.getTable());
			}
			setContent(createDays.getLayout());
		} else {
			if (deleteDays == null) {
				deleteDays = new DeleteDaysController(this, salaryTable.getTable());
			}
			setContent(deleteDays.getLayout());
		}
	}
}