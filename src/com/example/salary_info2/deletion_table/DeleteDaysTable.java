package com.example.salary_info2.deletion_table;

import java.util.Date;

import com.example.salary_info2.Calculations;
import com.example.salary_info2.SalaryInfoUI;
import com.example.salary_info2.VaadinTable;
import com.example.salary_info2.salary_table.SalaryTable;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class DeleteDaysTable extends VaadinTable {
	
	private SalaryInfoUI view;
	private SalaryTable salaryTable;
	
	public DeleteDaysTable(SalaryInfoUI view, SalaryTable salaryTable) {
		super("Wybierz przedzia� dat do usuni�cia");
		this.view = view;
		this.salaryTable = salaryTable;
		defineColumns();
		createColumns();
		setColumnAlignments(ALIGNMENT.CENTER);
		fillTable();
		setProperties();
	}
	
	public Date getStartDate() {
		return (Date) getContainerProperty(1, COLUMNS[0]).getValue();
	}
	
	public Date getEndDate() {
		return (Date) getContainerProperty(1, COLUMNS[1]).getValue();
	}

	@Override
	public <T> boolean addRow(T row) {
		try {
			DeleteDaysButton button = new DeleteDaysButton(view, salaryTable, this);
			addItem(new Object[] {
					Calculations.getFirstDayOfMonth(), Calculations.getLastDayOfMonth(), button
			}, 1);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			Notification.show("B��d dodawania wiersza tabeli Vaadin: " + ex);
			return false;
		}
	}

	@Override
	protected void defineColumns() {
		COLUMNS = new String[] {
				"Data pocz�tkowa",
				"Data ko�cowa",
				"Generuj"};
	}
	
	@Override
	protected void createColumns() {
		addContainerProperty(COLUMNS[0], Date.class, null);
		addContainerProperty(COLUMNS[1], Date.class, null);
		addContainerProperty(COLUMNS[2], Button.class, null);
	}
	
	private void fillTable() {
		addRow(null);
	}
}
