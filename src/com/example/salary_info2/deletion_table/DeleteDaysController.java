package com.example.salary_info2.deletion_table;

import com.example.salary_info2.Calculations;
import com.example.salary_info2.SalaryInfoUI;
import com.example.salary_info2.database.DayService;
import com.example.salary_info2.salary_table.SalaryTable;
import com.example.salary_info2.salary_table.SalaryTableLayout;

public class DeleteDaysController {
	
	private SalaryInfoUI view;
	private SalaryTable salaryTable;
	private SalaryTableLayout layout;
	
	public DeleteDaysController(SalaryInfoUI view, SalaryTable salaryTable) {
		this.view = view;
		this.salaryTable = salaryTable;
		init();
	}
	
	public SalaryTableLayout getLayout() {
		return layout;
	}
	
	private void init() {
		layout = new SalaryTableLayout();
		DeleteDaysTable deleteTable = new DeleteDaysTable(view, salaryTable);
		layout.addComponent(deleteTable);
	}
}
