package com.example.salary_info2.deletion_table;

import java.util.Calendar;
import java.util.Date;

import com.example.salary_info2.Calculations;
import com.example.salary_info2.SalaryInfoUI;
import com.example.salary_info2.ViewState;
import com.example.salary_info2.database.DayService;
import com.example.salary_info2.salary_table.SalaryTable;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class DeleteDaysButton extends Button {
	
	private SalaryTable salaryTable;
	private DeleteDaysTable deleteTable;
	
	public DeleteDaysButton(SalaryInfoUI view, SalaryTable salaryTable, DeleteDaysTable deleteTable) {
		super("OK");
		this.salaryTable = salaryTable;
		this.deleteTable = deleteTable;
		addClickListener(event -> {
			iterateDays();
			salaryTable.refreshFooter();
			view.CURRENT = ViewState.MAIN;
			view.refresh(null);
			Notification.show("Dni usuni�te.");
		});
	}
	
	private void iterateDays() {
		Date startDate = (Date) deleteTable.getContainerProperty(1, "Data pocz�tkowa").getValue();
		Date endDate = (Date) deleteTable.getContainerProperty(1, "Data ko�cowa").getValue();
		
		int daysCount = Calculations.daysBetween(startDate, endDate);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		
		DayService dayService = new DayService();
		for (int i = 0; i <= daysCount; i++) {
			salaryTable.removeRow(startDate);
			dayService.deleteDay(startDate);
			calendar.add(Calendar.DATE, 1);
			startDate = calendar.getTime();
		}
	}
}
