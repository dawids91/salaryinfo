package com.example.salary_info2;

public final class HTML {
	
	public static String lineBreak() {
		return "<br/><br/>";
	}
	
	public static String datesRange(String START_DATE, String END_DATE) {
		return "<p style='font-size:1.3em'>Przedzia� czasu:" + lineBreak() + START_DATE + " - " + END_DATE + "</p>";
	}
	
	public static String averageRate(String AVERAGE_RATE) {
		return "<i style='font-size:1.2em'>�rednia: " + AVERAGE_RATE + "z�</i>";
	}
	
	public static String totalHours(String TOTAL_HOURS) {
		return "<b style='font-size:1.5em;'>Suma: " + TOTAL_HOURS + "h</b>";
	}
	
	public static String averageHours(String AVERAGE_HOURS) {
		return "<i style='font-size:1.2em'>�rednia: " + AVERAGE_HOURS + "h</i>";
	}
	
	public static String totalBonus(String TOTAL_BONUS) {
		return "<b style='font-size:1.5em;'>Suma: " + TOTAL_BONUS + "z�</b>";
	}
	
	public static String averageBonus(String AVERAGE_BONUS) {
		return "<i style='font-size:1.2em'>�rednia: " + AVERAGE_BONUS + "z�</i>";
	}
	
	public static String totalSalary(String TOTAL_SALARY) {
		return "<b style='font-size:1.5em;color:blue;'>Suma: " + TOTAL_SALARY + "z�</b>";
	}
	
	public static String averageSalary(String AVERAGE_SALARY) {
		return "<i style='font-size:1.2em'>�rednia: " + AVERAGE_SALARY + "z�</i>";
	}
}
