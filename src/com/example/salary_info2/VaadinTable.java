package com.example.salary_info2;

import com.vaadin.ui.Table;

public abstract class VaadinTable extends Table {
	
	public String[] COLUMNS;

	public VaadinTable(String TABLE_NAME) {
		super(TABLE_NAME);
	}
	
	public abstract <T> boolean addRow(T row);
	
	protected abstract void defineColumns();
	
	protected abstract void createColumns();
	
	protected void setColumnAlignments(ALIGNMENT alignment) {
		int columnCount = this.getColumnAlignments().length;
		Align[] newAlignments = new Align[columnCount];
		switch(alignment) {
			case LEFT:
				for (int i = 0; i < columnCount; i++) {
					newAlignments[i] = Align.LEFT;
				}
				break;
			case CENTER:
				for (int i = 0; i < columnCount; i++) {
					newAlignments[i] = Align.CENTER;
				}
				break;
			case RIGHT:
				for (int i = 0; i < columnCount; i++) {
					newAlignments[i] = Align.RIGHT;
				}
				break;
			}
		this.setColumnAlignments(newAlignments);
	}
	
	protected void setProperties() {
		setPageLength(this.size());
		setEditable(true);
		setImmediate(true);
	}
	
	public void refreshTableLength() {
		setPageLength(this.size());
	}
	
	protected enum ALIGNMENT {LEFT, CENTER, RIGHT}
}
