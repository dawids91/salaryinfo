package com.example.salary_info2.creation_table;

import java.util.Calendar;
import java.util.Date;

import com.example.salary_info2.Calculations;
import com.example.salary_info2.SalaryInfoUI;
import com.example.salary_info2.ViewState;
import com.example.salary_info2.database.Day;
import com.example.salary_info2.salary_table.SalaryTable;
import com.vaadin.ui.Button;

public class CreateDaysButton extends Button {

	private SalaryTable salaryTable;
	private CreateDaysTable createTable;
	
	public CreateDaysButton(SalaryInfoUI view, SalaryTable salaryTable, CreateDaysTable createTable) {
		super("OK");
		this.salaryTable = salaryTable;
		this.createTable = createTable;
		addClickListener(event -> {
			iterateDays();
			salaryTable.refreshFooter();
			salaryTable.refreshTableLength();
			view.CURRENT = ViewState.MAIN;
			view.refresh(null);
		});
	}
	
	private void iterateDays() {
		Date startDate = createTable.getStartDate();
		Date endDate = createTable.getEndDate();
		float rate = createTable.getRate();
		float hours = createTable.getHours();
		float bonus = createTable.getBonus();
		
		int daysCount = Calculations.daysBetween(startDate, endDate);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		for (int i = 0; i <= daysCount; i++) {
			salaryTable.addRow(new Day(startDate, rate, hours, bonus));
			calendar.add(Calendar.DATE, 1);
			startDate = calendar.getTime();
		}
	}
}
