package com.example.salary_info2.creation_table;

import com.example.salary_info2.SalaryInfoUI;
import com.example.salary_info2.salary_table.SalaryTable;
import com.example.salary_info2.salary_table.SalaryTableLayout;

public class CreateDaysController {

	private SalaryInfoUI view;
	private SalaryTable salaryTable;
	private SalaryTableLayout layout;
	
	public CreateDaysController(SalaryInfoUI view, SalaryTable salaryTable) {
		this.view = view;
		this.salaryTable = salaryTable;
		init();
	}
	
	public SalaryTableLayout getLayout() {
		return layout;
	}
	
	private void init() {
		layout = new SalaryTableLayout();
		CreateDaysTable createTable = new CreateDaysTable(view, salaryTable);
		layout.addComponent(createTable);
	}
}
