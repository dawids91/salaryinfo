package com.example.salary_info2.creation_table;

import java.util.Date;

import com.example.salary_info2.Calculations;
import com.example.salary_info2.SalaryInfoUI;
import com.example.salary_info2.VaadinTable;
import com.example.salary_info2.salary_table.SalaryTable;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class CreateDaysTable extends VaadinTable {

	private SalaryInfoUI view;
	private SalaryTable salaryTable;
	
	public CreateDaysTable(SalaryInfoUI view, SalaryTable salaryTable) {
		super("Wybierz przedzia� dat do dodania");
		this.view = view;
		this.salaryTable = salaryTable;
		defineColumns();
		createColumns();
		setColumnAlignments(ALIGNMENT.CENTER);
		fillTable();
		setProperties();
	}
	
	public Date getStartDate() {
		return (Date) getContainerProperty(1, COLUMNS[0]).getValue();
	}
	
	public Date getEndDate() {
		return (Date) getContainerProperty(1, COLUMNS[1]).getValue();
	}
	
	public float getRate() {
		return (float) getContainerProperty(1, COLUMNS[2]).getValue();
	}
	
	public float getHours() {
		return (float) getContainerProperty(1, COLUMNS[3]).getValue();
	}
	
	public float getBonus() {
		return (float) getContainerProperty(1, COLUMNS[4]).getValue();
	}

	@Override
	public <T> boolean addRow(T row) {
		try {
			CreateDaysButton button = new CreateDaysButton(view, salaryTable, this);
			addItem(new Object[] {
					Calculations.getFirstDayOfMonth(), Calculations.getLastDayOfMonth(), 0f, 0f, 0f, button
			}, 1);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			Notification.show("B��d dodawania wiersza tabeli Vaadin: " + ex);
			return false;
		}
	}

	@Override
	protected void defineColumns() {
		COLUMNS = new String[] {
				"Data pocz�tkowa",
				"Data ko�cowa",
				"Stawka godzinowa [z�]",
				"Liczba godzin [h]",
				"Premia [z�]",
				"Zatwierd�"};
	}
	
	@Override
	protected void createColumns() {
		addContainerProperty(COLUMNS[0], Date.class, null);
		addContainerProperty(COLUMNS[1], Date.class, null);
		addContainerProperty(COLUMNS[2], Float.class, 0);
		addContainerProperty(COLUMNS[3], Float.class, 0);
		addContainerProperty(COLUMNS[4], Float.class, 0);
		addContainerProperty(COLUMNS[5], Button.class, null);
	}
	
	private void fillTable() {
		addRow(null);
	}
}
